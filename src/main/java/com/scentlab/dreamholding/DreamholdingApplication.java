package com.scentlab.dreamholding;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DreamholdingApplication {

    public static void main(String[] args) {
        SpringApplication.run(DreamholdingApplication.class, args);
    }

}
