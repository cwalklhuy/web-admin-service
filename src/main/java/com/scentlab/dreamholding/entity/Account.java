package com.scentlab.dreamholding.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Collection;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "account")
public class Account {
    @Id
    @Column(name = "username")
    private String username;
    @Column(name = "password", nullable = false)
    private String password;
    @Column(name = "full_name")
    private String fullName;
    @Column(name = "photo")
    private String photo;
    @Column(name = "gender")
    private int gender;
    @Column(name = "address")
    private String address;
    @Column(name = "is_active")
    private int isActive;

    @OneToMany(mappedBy = "username")
    private Collection<AccountRole> accountRoles;
}
