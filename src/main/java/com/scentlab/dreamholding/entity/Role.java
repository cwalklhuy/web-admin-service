package com.scentlab.dreamholding.entity;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Collection;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "role")
public class Role {
    @Id
    @GenericGenerator(name = "generator", strategy = "increment")
    @GeneratedValue(generator = "generator")
    @Column(name = "role_id")
    private int roleId;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "role")
    private Collection<AccountRole> accountRoles;
}
